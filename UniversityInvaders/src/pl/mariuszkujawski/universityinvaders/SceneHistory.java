package pl.mariuszkujawski.universityinvaders;

import org.andengine.entity.scene.Scene;

import pl.mariuszkujawski.universityinvaders.scenes.GameScene;
import pl.mariuszkujawski.universityinvaders.scenes.MainMenuScene;
import pl.mariuszkujawski.universityinvaders.scenes.SplashScene;
import pl.mariuszkujawski.universityinvaders.util.SizedStack;
import android.util.Log;

public class SceneHistory {
	protected static SizedStack<Scene> scenes = new SizedStack<Scene>(10);

	public static void back() {
		Scene whereToGo = scenes.pop();
		Log.e("BACK", "(" + scenes.size() + ") pop  " + whereToGo.getClass().getName());
		if (whereToGo == null) {
			whereToGo = new MainMenuScene();
		}
		
		if (whereToGo instanceof MainMenuScene) {
			scenes = new SizedStack<Scene>(10);
		}
		
		if (BaseActivity.getSharedInstance().mCurrentScene instanceof GameScene) {
			((GameScene) BaseActivity.getSharedInstance().mCurrentScene).detach();
		}
		BaseActivity.getSharedInstance().mCurrentScene = whereToGo;
		BaseActivity.getSharedInstance().getEngine().setScene(whereToGo);
	}

	public static void push(Scene scene) {
		if (scene instanceof SplashScene) {
			return;
		}
		
		scenes.push(scene);
		Log.e("BACK", "(" + scenes.size() + ") push " + scene.getClass().getName());
	}

}
