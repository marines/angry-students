package pl.mariuszkujawski.universityinvaders.level;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.TextMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ClickDetector;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;
import org.andengine.util.color.Color;

import pl.mariuszkujawski.universityinvaders.BaseActivity;
import pl.mariuszkujawski.universityinvaders.scenes.GameScene;
import android.util.Log;

public class StageSelectScene extends Scene implements IScrollDetectorListener, IOnSceneTouchListener, IOnMenuItemClickListener {

	BaseActivity activity;

	private MenuScene menuChildScene = null;
	private final int STAGE_ONE = 1;
	private final int STAGE_TWO = 2;
	private final int STAGE_THREE = 3;
	private final int STAGE_FOUR = 4;
	private final int STAGE_FIVE = 5;
	private final int STAGE_SIX = 6;
	private final int STAGE_SEVEN = 7;

	// Scrolling
	private SurfaceScrollDetector scrollDetector = null;
	private ClickDetector clickDetector = null;

	private boolean swipe = false;

	public StageSelectScene(BaseActivity activity) {
		super();
		this.activity = activity;

		createScene();
	}

	public void createScene() {

		createBackground();
		createMenuChildScene();

		Text chooseTxt = new Text(100, 100, activity.mFont, "Pick semester", activity.getVertexBufferObjectManager());
		attachChild(chooseTxt);

		this.setOnSceneTouchListener(this);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);

	}

	private void createBackground() {
		setBackground(new Background(Color.WHITE));

		// Rectangle r = new Rectangle(0, 0, 200, 200, getVbom());
		// this.attachChild(r);
	}

	private void createMenuChildScene() {

		/*
		 * We are creating two sprite menu items, for our two buttons (play &
		 * options) we also use scale menu item decorator, which makes our menu
		 * items animated (they are being scaled up while touched) it's a really
		 * easy effect, you may easily create your own, it's more than enough
		 * for the purpose of this tutorial though. We are also adding those
		 * menu items to the menu scene, and positioning those menu items, to
		 * looks good with our background.
		 */
		menuChildScene = new MenuScene(activity.mCamera);
		menuChildScene.setPosition(0, 0);

		final IMenuItem stage1MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_ONE, activity.mFont, "First",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage2MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_TWO, activity.mFont, "Second",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage3MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_THREE, activity.mFont, "Third",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage4MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_FOUR, activity.mFont, "Fourth",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage5MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_FIVE, activity.mFont, "Fifth",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage6MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_FIVE, activity.mFont, "Sixth",
				activity.getVertexBufferObjectManager()), 1.0f, 1);
		final IMenuItem stage7MenuItem = new ScaleMenuItemDecorator(new TextMenuItem(STAGE_FIVE, activity.mFont, "Seventh",
				activity.getVertexBufferObjectManager()), 1.0f, 1);

		stage1MenuItem.setScale(1.0f);
		stage2MenuItem.setScale(1.0f);
		stage3MenuItem.setScale(1.0f);
		stage4MenuItem.setScale(1.0f);
		stage5MenuItem.setScale(1.0f);
		stage6MenuItem.setScale(1.0f);
		stage7MenuItem.setScale(1.0f);

		menuChildScene.addMenuItem(stage1MenuItem);
		menuChildScene.addMenuItem(stage2MenuItem);
		menuChildScene.addMenuItem(stage3MenuItem);
		menuChildScene.addMenuItem(stage4MenuItem);
		menuChildScene.addMenuItem(stage5MenuItem);
		menuChildScene.addMenuItem(stage6MenuItem);
		menuChildScene.addMenuItem(stage7MenuItem);

		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);

		stage1MenuItem.setPosition(50, 250);
		stage2MenuItem.setPosition(300, 250);
		stage3MenuItem.setPosition(550, 250);
		menuChildScene.attachChild(createSoonLabel(550 + stage2MenuItem.getWidth() - 50, 250));
		stage4MenuItem.setPosition(800, 250);
		menuChildScene.attachChild(createSoonLabel(800 + stage2MenuItem.getWidth() - 50, 250));
		stage5MenuItem.setPosition(1050, 250);
		menuChildScene.attachChild(createSoonLabel(1050 + stage2MenuItem.getWidth() - 50, 250));
		stage6MenuItem.setPosition(1300, 250);
		menuChildScene.attachChild(createSoonLabel(1300 + stage2MenuItem.getWidth() - 50, 250));
		stage7MenuItem.setPosition(1550, 250);
		menuChildScene.attachChild(createSoonLabel(1550 + stage2MenuItem.getWidth() - 50, 250));

		// menu click listener, we will use it, to execute certain actions after
		// pressing menu buttons.
		menuChildScene.setOnMenuItemClickListener(this);

		setChildScene(menuChildScene);
	}

	public Sprite createSoonLabel(float f, float y) {
		BitmapTexture titleTex;
		ITextureRegion titleRegion = null;
		try {
			titleTex = new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return activity.getAssets().open("coming-soon.png");
				}
			});

			titleTex.load();
			titleRegion = TextureRegionFactory.extractFromTexture(titleTex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Sprite title = new Sprite(f, y, titleRegion, activity.getVertexBufferObjectManager());
		return title;
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
		switch (pMenuItem.getID()) {
		case STAGE_ONE:
			activity.setCurrentScene(new GameScene(1));
			break;
		case STAGE_TWO:
			activity.setCurrentScene(new GameScene(2));
			break;
		}

		return true;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

		if (this.scrollDetector == null) {
			this.scrollDetector = new SurfaceScrollDetector(this);
		}

		// scrollDetector.onTouchEvent(pSceneTouchEvent);
		scrollDetector.onManagedTouchEvent(pSceneTouchEvent);
		Log.d("TAG", "ON SCENE TOUCHED");

		int action = pSceneTouchEvent.getAction();

		switch (action) {
		case TouchEvent.ACTION_DOWN:
			swipe = true;
			break;
		case TouchEvent.ACTION_UP:
			swipe = false;
			break;

		}

		return true;
	}

	@Override
	public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {

	}

	@Override
	public void onScroll(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {

		// pScollDetector.setTriggerScrollMinimumDistance(-50);

		if (swipe) {
			if (getChildScene().getX() + pDistanceX < 0 && getChildScene().getX() + pDistanceX > -1000) {
				getChildScene().setX(getChildScene().getX() + pDistanceX);
				Log.e("d", "" + (getChildScene().getX() + pDistanceX));
			}
		}

	}

	@Override
	public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID, float pDistanceX, float pDistanceY) {

	}

}