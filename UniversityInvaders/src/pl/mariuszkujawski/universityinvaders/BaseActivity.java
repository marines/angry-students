package pl.mariuszkujawski.universityinvaders;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import pl.mariuszkujawski.universityinvaders.scenes.GameScene;
import pl.mariuszkujawski.universityinvaders.scenes.MainMenuScene;
import pl.mariuszkujawski.universityinvaders.scenes.SplashScene;

import android.graphics.Typeface;
import android.util.Log;

public class BaseActivity extends SimpleBaseGameActivity {

	public static final int CAMERA_WIDTH = 800;
	public static final int CAMERA_HEIGHT = 480;

	public Font mFont;
	public Camera mCamera;

	public Scene mCurrentScene;
	public static BaseActivity instance;

	@Override
	public EngineOptions onCreateEngineOptions() {
		instance = this;
		mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR,
				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), mCamera);

	}

	@Override
	protected void onCreateResources() {
		mFont = FontFactory.create(this.getFontManager(),
				this.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 42);
		mFont.load();
	}
	
	@Override
	public void onBackPressed() {
		Log.e("BACK", "wci�ni�to back");
		if (mCurrentScene instanceof MainMenuScene) {
			mCurrentScene = null;
		    SensorListener.instance = null;
		    super.onBackPressed();
		    return;
		}
		
		SceneHistory.back();
	}

	@Override
	protected Scene onCreateScene() {
		mEngine.registerUpdateHandler(new FPSLogger());
		mCurrentScene = new SplashScene();
		return mCurrentScene;
	}

	public static BaseActivity getSharedInstance() {
		return instance;
	}

	public void setCurrentScene(Scene scene) {
		if (mCurrentScene instanceof GameScene && scene instanceof GameScene) {
			return;
		}
		SceneHistory.push(mCurrentScene);
		mCurrentScene = scene;
		getEngine().setScene(mCurrentScene);
	}

}
