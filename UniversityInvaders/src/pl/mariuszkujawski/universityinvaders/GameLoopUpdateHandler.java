package pl.mariuszkujawski.universityinvaders;

import org.andengine.engine.handler.IUpdateHandler;

import pl.mariuszkujawski.universityinvaders.scenes.GameScene;

public class GameLoopUpdateHandler implements IUpdateHandler {

	@Override
	public void onUpdate(float pSecondsElapsed) {
		// do dupy
		((GameScene) BaseActivity.getSharedInstance().mCurrentScene).moveShip();
		((GameScene) BaseActivity.getSharedInstance().mCurrentScene).cleaner(pSecondsElapsed);

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

}
