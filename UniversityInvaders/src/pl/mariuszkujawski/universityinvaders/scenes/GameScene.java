package pl.mariuszkujawski.universityinvaders.scenes;

import java.util.Iterator;
import java.util.LinkedList;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.IEntityFactory;
import org.andengine.entity.particle.ParticleSystem;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.particle.modifier.RotationParticleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.color.Color;

import pl.mariuszkujawski.universityinvaders.BaseActivity;
import pl.mariuszkujawski.universityinvaders.BulletPool;
import pl.mariuszkujawski.universityinvaders.EnemyPool;
import pl.mariuszkujawski.universityinvaders.GameLoopUpdateHandler;
import pl.mariuszkujawski.universityinvaders.R;
import pl.mariuszkujawski.universityinvaders.SensorListener;
import pl.mariuszkujawski.universityinvaders.layers.EnemyLayer;
import pl.mariuszkujawski.universityinvaders.objects.Bullet;
import pl.mariuszkujawski.universityinvaders.objects.Enemy;
import pl.mariuszkujawski.universityinvaders.objects.Ship;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

public class GameScene extends Scene implements IOnSceneTouchListener {
	public Ship ship;
	Camera mCamera;
	public float accelerometerSpeedX = 0.0f;
	private SensorManager sensorManager;
	public LinkedList<Bullet> bulletList;
	public int bulletCount;
	public int missCount;
	public long timePassed = 0;
	public int semester = 1;
	private Text time;
	private Text accuracy;
	public float dtSum = 0;

	public GameScene(int semester) {
		this.semester = semester;
		bulletList = new LinkedList<Bullet>();
		setBackground(new Background(Color.WHITE));
		mCamera = BaseActivity.getSharedInstance().mCamera;
		ship = Ship.getSharedInstance();
		attachChild(ship.sprite);

		BaseActivity.getSharedInstance().setCurrentScene(this);
		sensorManager = (SensorManager) BaseActivity.getSharedInstance().getSystemService(BaseGameActivity.SENSOR_SERVICE);
		SensorListener.getSharedInstance();
		SensorListener.getSharedInstance().setScene(this);
		sensorManager.registerListener(SensorListener.getSharedInstance(), sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_GAME);

		setOnSceneTouchListener(this);

		addIndicators();
		
		attachChild(makeEnemyLayer());

		resetValues();
	}

	private void addIndicators() {
		time = new Text(10, 10, BaseActivity.getSharedInstance().mFont, "Time: ", 100, BaseActivity.getSharedInstance().getVertexBufferObjectManager());
		accuracy = new Text(BaseActivity.CAMERA_WIDTH - 10 - 240, 10, BaseActivity.getSharedInstance().mFont, "Acc: ", 100, BaseActivity.getSharedInstance().getVertexBufferObjectManager());
		
		attachChild(time);
		attachChild(accuracy);
	}

	protected EnemyLayer makeEnemyLayer() {
		switch (semester) {
		case 1:
			return new EnemyLayer(3);
		case 2:
			return new EnemyLayer(9);
		default:
			return new EnemyLayer(1);
		}

	}

	public void moveShip() {
		ship.moveShip(accelerometerSpeedX);
	}

	public void cleaner(float dt) {
		synchronized (this) {
			dtSum += dt;
			Log.e("BACK", "czas: " + dtSum);
			if (dtSum > 0.2) {
				updateIndicators();
				dtSum = 0;
			}
			
			// if all Enemies are killed
			if (EnemyLayer.isEmpty()) {
				setChildScene(new ResultScene(mCamera));
				clearUpdateHandlers();
			}

			Iterator<Enemy> eIt = EnemyLayer.getIterator();
			while (eIt.hasNext()) {
				Enemy e = eIt.next();
				Iterator<Bullet> it = bulletList.iterator();
				while (it.hasNext()) {
					Bullet b = it.next();
					if (b.sprite.getY() <= -b.sprite.getHeight()) {
						missCount++;
						BulletPool.sharedBulletPool().recyclePoolItem(b);
						it.remove();
						continue;
					}

					if (b.sprite.collidesWith(e.sprite)) {
						if (!e.gotHit()) {
							createExplosion(e.sprite.getX(), e.sprite.getY(), e.sprite.getParent(), BaseActivity.getSharedInstance(), 15, 1,
									new Color(192.0f / 255, 192.0f / 255, 192.0f / 255));
							EnemyPool.sharedEnemyPool().recyclePoolItem(e);
							eIt.remove();
						} else {
							createExplosion(e.sprite.getX(), e.sprite.getY(), e.sprite.getParent(), BaseActivity.getSharedInstance(), 5, 1,
									new Color(196f / 255, 142f / 255, 56f / 255));
						}
						BulletPool.sharedBulletPool().recyclePoolItem(b);
						it.remove();
						break;
					}
				}
			}
		}
	}

	private void updateIndicators() {
		float accuracy = 1 - (float) missCount / bulletCount;
		if (Float.isNaN(accuracy))
			accuracy = 0;
		accuracy *= 100;
		
		long timePassed2 = (System.currentTimeMillis() - timePassed) / 1000;
		
		this.time.setText("Time: " + String.format("%d:%02d:%02d", timePassed2/3600, (timePassed2%3600)/60, (timePassed2%60)));
		this.accuracy.setText("Acc: " + String.format("%.2f", accuracy) + "%");
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		synchronized (this) {
			if (pSceneTouchEvent.isActionDown()) {
				ship.shoot();
			}
		}
		return true;
	}

	// method to reset values and restart the game
	public void resetValues() {
		missCount = 0;
		bulletCount = 0;
		timePassed = System.currentTimeMillis();
		ship.restart();
		EnemyLayer.purgeAndRestart();
		clearChildScene();
		registerUpdateHandler(new GameLoopUpdateHandler());
	}

	public void detach() {
		clearUpdateHandlers();
		for (Bullet b : bulletList) {
			BulletPool.sharedBulletPool().recyclePoolItem(b);
		}
		bulletList.clear();
		detachChildren();
		Ship.instance = null;
		EnemyPool.instance = null;
		BulletPool.instance = null;
	}

	private void createExplosion(final float posX, final float posY, final IEntity target, final SimpleBaseGameActivity activity, int mNumPart,
			float mTimePart, final Color color) {

		PointParticleEmitter particleEmitter = new PointParticleEmitter(posX, posY);
		IEntityFactory recFact = new IEntityFactory() {
			@Override
			public Rectangle create(float pX, float pY) {
				Rectangle rect = new Rectangle(posX, posY, 10, 10, activity.getVertexBufferObjectManager());
				rect.setColor(color);
				return rect;
			}
		};
		final ParticleSystem particleSystem = new ParticleSystem(recFact, particleEmitter, 500, 500, mNumPart);

		particleSystem.addParticleInitializer(new VelocityParticleInitializer(-50, 50, -50, 50));

		particleSystem.addParticleModifier(new AlphaParticleModifier(0, 0.6f * mTimePart, 1, 0));
		particleSystem.addParticleModifier(new RotationParticleModifier(0, mTimePart, 0, 360));

		target.attachChild(particleSystem);

		target.registerUpdateHandler(new TimerHandler(mTimePart, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				particleSystem.detachSelf();
				target.sortChildren();
				target.unregisterUpdateHandler(pTimerHandler);
			}
		}));
	}
}
