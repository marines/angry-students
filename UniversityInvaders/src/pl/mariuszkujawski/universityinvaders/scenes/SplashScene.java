package pl.mariuszkujawski.universityinvaders.scenes;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import pl.mariuszkujawski.universityinvaders.BaseActivity;
import pl.mariuszkujawski.universityinvaders.R;

public class SplashScene extends Scene {

	BaseActivity activity;

	public SplashScene() {
		setBackground(new Background(1.0f, 1.0f, 1.0f));
		activity = BaseActivity.getSharedInstance();


		BitmapTexture mTexture;
		ITextureRegion mFaceTextureRegion = null;
		try {
			mTexture = new BitmapTexture(activity.getTextureManager(),
					new IInputStreamOpener() {
						@Override
						public InputStream open() throws IOException {
							return activity.getAssets().open("ein.png");
						}
					});

			mTexture.load();
			mFaceTextureRegion = TextureRegionFactory
					.extractFromTexture(mTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Sprite ein = new Sprite(BaseActivity.CAMERA_WIDTH + mFaceTextureRegion.getWidth(), (BaseActivity.CAMERA_HEIGHT - mFaceTextureRegion.getHeight())/2,
				mFaceTextureRegion, activity.getVertexBufferObjectManager());
		attachChild(ein);
		ein.registerEntityModifier(new MoveXModifier(1, BaseActivity.CAMERA_WIDTH + mFaceTextureRegion.getWidth(), BaseActivity.CAMERA_WIDTH / 2));
		
		
		
		BitmapTexture titleTex;
		ITextureRegion titleRegion = null;
		try {
			titleTex = new BitmapTexture(activity.getTextureManager(),
					new IInputStreamOpener() {
						@Override
						public InputStream open() throws IOException {
							return activity.getAssets().open("title.png");
						}
					});

			titleTex.load();
			titleRegion = TextureRegionFactory
					.extractFromTexture(titleTex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Sprite title = new Sprite(BaseActivity.CAMERA_WIDTH / 8, BaseActivity.CAMERA_HEIGHT / 8,
				titleRegion, activity.getVertexBufferObjectManager());
		attachChild(title);
		title.registerEntityModifier(new MoveXModifier(1, -title.getWidth(),
				BaseActivity.CAMERA_WIDTH / 8));

		
		
		loadResources();
	}

	public void loadResources() {
		DelayModifier dMod = new DelayModifier(2) {
			@Override
			protected void onModifierFinished(IEntity pItem) {
				activity.setCurrentScene(new MainMenuScene());
			}
		};
		registerEntityModifier(dMod);
	}
}
