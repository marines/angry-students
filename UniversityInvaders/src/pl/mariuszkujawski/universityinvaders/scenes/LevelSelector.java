package pl.mariuszkujawski.universityinvaders.scenes;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.TextMenuItem;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import pl.mariuszkujawski.universityinvaders.BaseActivity;

public abstract class LevelSelector extends MenuScene implements IOnMenuItemClickListener {
	BaseActivity activity;
	final int MENU_QP = 0;
	final int MENU_CAR = 1;

	public LevelSelector() {
		super(BaseActivity.getSharedInstance().mCamera);
		activity = BaseActivity.getSharedInstance();

		setBackground(new Background(1.0f, 1.0f, 1.0f));

		IMenuItem quickPlayButton = new TextMenuItem(MENU_QP, activity.mFont, "QUICK PLAY", activity.getVertexBufferObjectManager());
		float w1 = mCamera.getWidth() / 20;
		float h1 = (float) (mCamera.getHeight() / 1.3 - quickPlayButton.getHeight() / 2) - 20;
		quickPlayButton.setPosition(w1, h1);
		addMenuItem(quickPlayButton);

		IMenuItem careerButton = new TextMenuItem(MENU_QP, activity.mFont, "CAREER", activity.getVertexBufferObjectManager());
		float h2 = (float) (mCamera.getHeight() / 1.1 - careerButton.getHeight() / 2) - 20;
		careerButton.setPosition(w1, h2);
		addMenuItem(careerButton);

		BitmapTexture mTexture;
		ITextureRegion mFaceTextureRegion = null;
		try {
			mTexture = new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return activity.getAssets().open("ein.png");
				}
			});

			mTexture.load();
			mFaceTextureRegion = TextureRegionFactory.extractFromTexture(mTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Sprite ein = new Sprite(BaseActivity.CAMERA_WIDTH / 2, (BaseActivity.CAMERA_HEIGHT - mFaceTextureRegion.getHeight()) / 2, mFaceTextureRegion,
				activity.getVertexBufferObjectManager());
		attachChild(ein);

		BitmapTexture titleTex;
		ITextureRegion titleRegion = null;
		try {
			titleTex = new BitmapTexture(activity.getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return activity.getAssets().open("title.png");
				}
			});

			titleTex.load();
			titleRegion = TextureRegionFactory.extractFromTexture(titleTex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Sprite title = new Sprite(BaseActivity.CAMERA_WIDTH / 8, BaseActivity.CAMERA_HEIGHT / 8, titleRegion, activity.getVertexBufferObjectManager());
		attachChild(title);

		setOnMenuItemClickListener(this);

	}

	@Override
	abstract public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY);
}
