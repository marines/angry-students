package pl.mariuszkujawski.universityinvaders.scenes;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.scene.CameraScene;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;

import pl.mariuszkujawski.universityinvaders.BaseActivity;
import pl.mariuszkujawski.universityinvaders.R;
import android.graphics.Typeface;

public class ResultScene extends CameraScene implements IOnSceneTouchListener {

	boolean done;
	boolean done2;
	BaseActivity activity;

	public ResultScene(Camera mCamera) {
		super(mCamera);
		activity = BaseActivity.getSharedInstance();
		setBackgroundEnabled(false);
		GameScene scene = (GameScene) activity.mCurrentScene;
		float accuracy = 1 - (float) scene.missCount / scene.bulletCount;
		if (Float.isNaN(accuracy))
			accuracy = 0;
		accuracy *= 100;
		String accuracyString = activity.getString(R.string.accuracy) + ": "
				+ String.format("%.2f", accuracy) + "%";
		long timePassed = (System.currentTimeMillis() - scene.timePassed) / 1000;
		String timeString = activity.getString(R.string.accuracy) + ": "
				+ String.format("%d:%02d:%02d", timePassed/3600, (timePassed%3600)/60, (timePassed%60));
		Text result = new Text(0, 0, activity.mFont,
				"You have won the Internets!\n                 Congrats!", BaseActivity
						.getSharedInstance().getVertexBufferObjectManager());

		final int x = (int) (mCamera.getWidth() / 2 - result.getWidth() / 2);
		final int y = (int) (mCamera.getHeight() / 2 - result.getHeight() / 2);

		done = false;
		result.setPosition(x, mCamera.getHeight() + result.getHeight());
		MoveYModifier mod = new MoveYModifier(5, result.getY(), y) {
			@Override
			protected void onModifierFinished(IEntity pItem) {
				done = true;
			}
		};
		MoveYModifier mod2 = new MoveYModifier(5, result.getY() + result.getHeight() + 100, y + result.getHeight()+30) {
			@Override
			protected void onModifierFinished(IEntity pItem) {
				done2 = true;
			}
		};
		attachChild(result);
		
		
		Font mFont = FontFactory.create(BaseActivity.getSharedInstance().getFontManager(),
				BaseActivity.getSharedInstance().getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 18);
		mFont.load();
		Text next = new Text(10,0, mFont, "Touch screen to play again. Touch back button to go to menu.", BaseActivity.getSharedInstance().getVertexBufferObjectManager());
		next.setPosition(150, mCamera.getHeight() + result.getHeight() + next.getHeight() + 200);
		attachChild(next);
		
		result.registerEntityModifier(mod);
		next.registerEntityModifier(mod2);
		
		setOnSceneTouchListener(this);
	}

	@Override
	public boolean onSceneTouchEvent(Scene arg0, TouchEvent arg1) {
		if (!done)
		    return true;
		if (!done2)
		    return true;
		((GameScene) activity.mCurrentScene).resetValues();
		return false;
	}

}
