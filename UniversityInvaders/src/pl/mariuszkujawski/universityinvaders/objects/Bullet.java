package pl.mariuszkujawski.universityinvaders.objects;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import pl.mariuszkujawski.universityinvaders.BaseActivity;

public class Bullet {
	public Sprite sprite;

	public Bullet() {
		BitmapTexture mTexture;
		ITextureRegion mFaceTextureRegion = null;
		try {
			mTexture = new BitmapTexture(BaseActivity.getSharedInstance().getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return BaseActivity.getSharedInstance().getAssets().open("question.png");
				}
			});

			mTexture.load();
			mFaceTextureRegion = TextureRegionFactory.extractFromTexture(mTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sprite = new Sprite(0, 0, mFaceTextureRegion, BaseActivity.getSharedInstance().getVertexBufferObjectManager());
	}
}
