package pl.mariuszkujawski.universityinvaders.objects;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import pl.mariuszkujawski.universityinvaders.BaseActivity;
import pl.mariuszkujawski.universityinvaders.BulletPool;
import pl.mariuszkujawski.universityinvaders.scenes.GameScene;

public class Ship {
	public Sprite sprite;
	public static Ship instance;
	Camera mCamera;
	private boolean moveable = true;

	public static Ship getSharedInstance() {
		if (instance == null)
			instance = new Ship();
		return instance;
	}

	private Ship() {
		BitmapTexture mTexture;
		ITextureRegion mFaceTextureRegion = null;
		try {
			mTexture = new BitmapTexture(BaseActivity.getSharedInstance().getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return BaseActivity.getSharedInstance().getAssets().open("prof.png");
				}
			});

			mTexture.load();
			mFaceTextureRegion = TextureRegionFactory.extractFromTexture(mTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sprite = new Sprite(0, 0, mFaceTextureRegion, BaseActivity.getSharedInstance().getVertexBufferObjectManager());
		sprite.setScale((float) 0.7);
		
		mCamera = BaseActivity.getSharedInstance().mCamera;
		sprite.setPosition(mCamera.getWidth() / 2 - sprite.getWidth() / 2,
				mCamera.getHeight() - sprite.getHeight() - 10);
	}

	public void moveShip(float accelerometerSpeedX) {
		if (!moveable)
			return;

		if (accelerometerSpeedX != 0) {
			int leftEdge = 0;
			int rightEdge = (int) (mCamera.getWidth() - (int) sprite.getWidth());
			float newX;

			if (sprite.getX() >= leftEdge) {
				newX = sprite.getX() + accelerometerSpeedX;
			} else {
				newX = leftEdge;
			}

			if (newX <= rightEdge) {
				newX = sprite.getX() + accelerometerSpeedX;
			} else {
				newX = rightEdge;
			}

			if (newX < leftEdge) {
				newX = leftEdge;
			} else if (newX > rightEdge) {
				newX = rightEdge;
			}

			sprite.setPosition(newX, sprite.getY());
		}
	}
	
	public void shoot() {
		if (!moveable)
	        return;
	    GameScene scene = (GameScene) BaseActivity.getSharedInstance().mCurrentScene;
	 
	    Bullet b = BulletPool.sharedBulletPool().obtainPoolItem();
	    b.sprite.setPosition(sprite.getX() + sprite.getWidth() / 2,
	        sprite.getY());
	    MoveYModifier mod = new MoveYModifier(1.5f, b.sprite.getY(),
	        -b.sprite.getHeight());
	 
	    b.sprite.setVisible(true);
	    b.sprite.detachSelf();
	    scene.attachChild(b.sprite);
	    scene.bulletList.add(b);
	    b.sprite.registerEntityModifier(mod);
	    scene.bulletCount++;
	}
	
	// resets the ship to the middle of the screen
	public void restart() {
	    moveable = false;
	    Camera mCamera = BaseActivity.getSharedInstance().mCamera;
	    MoveXModifier mod = new MoveXModifier(0.2f, sprite.getX(),
	        mCamera.getWidth() / 2 - sprite.getWidth() / 2) {
	        @Override
	        protected void onModifierFinished(IEntity pItem) {
	            super.onModifierFinished(pItem);
	            moveable = true;
	        }
	    };
	    sprite.registerEntityModifier(mod);
	}
}
