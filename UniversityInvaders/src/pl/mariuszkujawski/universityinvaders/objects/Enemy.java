package pl.mariuszkujawski.universityinvaders.objects;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import pl.mariuszkujawski.universityinvaders.BaseActivity;

public class Enemy {
	public Sprite sprite;
	public int hp;
	// the max health for each enemy
	protected final int MAX_HEALTH = 2;

	public Enemy() {
		BitmapTexture mTexture;
		ITextureRegion mFaceTextureRegion = null;
		try {
			mTexture = new BitmapTexture(BaseActivity.getSharedInstance().getTextureManager(), new IInputStreamOpener() {
				@Override
				public InputStream open() throws IOException {
					return BaseActivity.getSharedInstance().getAssets().open("lazy-college-senior.png");
				}
			});

			mTexture.load();
			mFaceTextureRegion = TextureRegionFactory.extractFromTexture(mTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sprite = new Sprite(0, 0, mFaceTextureRegion, BaseActivity.getSharedInstance().getVertexBufferObjectManager());

		init();
	}

	// method for initializing the Enemy object , used by the constructor and
	// the EnemyPool class
	public void init() {
		hp = MAX_HEALTH;
		//sprite.registerEntityModifier(new LoopEntityModifier(new RotationModifier(5, 0, 360)));
	}

	public void clean() {
		sprite.clearEntityModifiers();
		sprite.clearUpdateHandlers();
	}

	// method for applying hit and checking if enemy died or not
	// returns false if enemy died
	public boolean gotHit() {
		synchronized (this) {
			hp--;
			if (hp == 1) {
				sprite.setAlpha((float) 0.5);
			}
			if (hp <= 0)
				return false;
			else
				return true;
		}
	}
}
